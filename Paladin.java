
public class Paladin extends Knight {

	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
	}

	@Override
	public double getCombatScore() {
		int baseHp = getBaseHp();
		if (Utility.isFibonacci(baseHp)) {
			int nth = Utility.getNthPositionFibonacci(baseHp);
			if (nth > 2)
				return 1000 + nth;
		}
		return 3 * baseHp;
	}
}
