public class Utility {

	/**
	 * Test whether a specific number is a prime number.
	 * 
	 * @param num the number
	 * @return <code>true</code> if <code>num</code> is a prime number.
	 */
	public static boolean isPrime(int num) {
		if (num < 2) {
			return false;
		}
		for (int i = 2; i < num / 2; i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Test whether a specific number is a square number.
	 * 
	 * @param num the number
	 * @return <code>true</code> if <code>num</code> is a square number.
	 */
	public static boolean isSquare(int num) {
		int x = (int) Math.sqrt(num);
		return x * x == num;
	}

	public static boolean isPerfectSquare(int x) {
		int s = (int) Math.sqrt(x);
		return (s * s == x);
	}

	// Returns true if n is a Fibonacci Number, else false
	public static boolean isFibonacci(int n) {
		// n is Fibonacci if one of 5*n*n + 4 or 5*n*n - 4 or both
		// is a perfect square
		return isPerfectSquare(5 * n * n + 4) || isPerfectSquare(5 * n * n - 4);
	}

	public static int fibonacci(int number) {
		if ((number == 0) || (number == 1)) // base cases
			return number;
		return fibonacci(number - 1) + fibonacci(number - 2);
	}

	public static int getNthPositionFibonacci(int n) {
		int i = 0;
		while (true) {
			if (n == fibonacci(i))
				return i;
			i++;
		}
	}
}
