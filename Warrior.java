public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
	}

	@Override
	public double getCombatScore() {
		if (Utility.isPrime(Battle.GROUND))
			return 2 * getBaseHp();
		if (getWp() == 1)
			return getBaseHp();
		else
			return (double) getBaseHp() / 10;
	}
}
